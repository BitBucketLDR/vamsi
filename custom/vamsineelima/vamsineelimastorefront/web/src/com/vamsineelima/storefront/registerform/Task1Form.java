/**
 *
 */
package com.vamsineelima.storefront.registerform;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;


/**
 * @author DELL
 *
 */
public class Task1Form extends RegisterForm
{
	private String phonenumber;
	private String dob;
	private String age;

	/**
	 * @return the age
	 */
	public String getAge()
	{
		return age;
	}

	/**
	 * @param age
	 *           the age to set
	 */
	public void setAge(final String age)
	{
		this.age = age;
	}

	/**
	 * @return the phonenumber
	 */
	public String getPhonenumber()
	{
		return phonenumber;
	}

	/**
	 * @param phonenumber
	 *           the phonenumber to set
	 */
	public void setPhonenumber(final String phonenumber)
	{
		this.phonenumber = phonenumber;
	}

	/**
	 * @return the dob
	 */
	public String getDob()
	{
		return dob;
	}

	/**
	 * @param dob
	 *           the dob to set
	 */
	public void setDob(final String dob)
	{
		this.dob = dob;
	}
}
