/**
 *
 */
package com.vamsineelima.storefront.registerform;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;


/**
 * @author DELL
 *
 */
@Component("registerFormValidator")
public class RegisterFormValidator extends RegistrationValidator
{

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final Task1Form task1Form = (Task1Form) object;
		final String titleCode = task1Form.getTitleCode();
		final String firstName = task1Form.getFirstName();
		final String lastName = task1Form.getLastName();
		final String email = task1Form.getEmail();
		final String pwd = task1Form.getPwd();
		final String checkPwd = task1Form.getCheckPwd();
		final String dob = task1Form.getDob();
		final String phoneNumber = task1Form.getPhonenumber();

		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);
		validatedob(dob, errors);
		validatephonenumber(phoneNumber, errors);

	}


	private void validatephonenumber(final String phoneNumber, final Errors errors)
	{
		if (StringUtils.isEmpty(phoneNumber))
		{
			errors.rejectValue("phonenumber", "register.phonenumber.invaild");
		}
	}

	/**
	 * @param dob
	 * @param errors
	 */
	private void validatedob(final String dob, final Errors errors)
	{

		if (StringUtils.isEmpty(dob))
		{
			errors.rejectValue("dob", "register.dob.invaild");
		}

	}

}
