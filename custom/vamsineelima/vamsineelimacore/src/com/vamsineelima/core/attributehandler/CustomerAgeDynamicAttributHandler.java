/**
 *
 */
package com.vamsineelima.core.attributehandler;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Calendar;
import java.util.Date;


/**
 * @author DELL
 *
 */
public class CustomerAgeDynamicAttributHandler implements DynamicAttributeHandler<Integer, CustomerModel>
{



	Integer age = 0;
	private Object val;


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.
	 * AbstractItemModel)
	 */
	@Override
	public Integer get(final CustomerModel model)
	{
		try
		{
			final Date date = model.getDob();
			final Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			final int year = cal.get(Calendar.YEAR);
			final int year1 = Calendar.getInstance().get(Calendar.YEAR);
			age = year1 - year;
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		return Integer.valueOf(age);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.
	 * AbstractItemModel, java.lang.Object)
	 */
	@Override
	public void set(final CustomerModel arg0, final Integer arg1)
	{
		if (val != null)
		{
			throw new UnsupportedOperationException();
		}

	}

}
